const express = require('express');
const path = require('path');
const mongoose = require('mongoose');

const db_config = require('./config/db.config');

const users = require('./routes/users');
const quizzes = require('./routes/quizzes');
const registerSocketEvents = require('./sockets/eventSetup');

mongoose.connection.on('error', (err) => {
    console.error(`Database error ${err}`);
});
mongoose.connection.on('connected', () => {
    console.log(`Connected to database ${db_config.url}`);
});
mongoose.connection.on('disconnected', () => {
    console.error(`Disconnected from database ${db_config.url}`);
});
mongoose.connect(db_config.url, {
    "useNewUrlParser": true,
    "useUnifiedTopology": true,
    "useCreateIndex": true,
    "useFindAndModify": false
});

const app = express();
const port = process.env.PORT || 3000;

/* CORS */
if(process.env.NODE_ENV !== 'production') {
    app.use(function(req, res, next) {
        res.header('Access-Control-Allow-Origin', 'http://localhost:4200');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
        res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
        next();
    });
}

app.use(express.static('public'));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use('/api/users', users);
app.use('/api/quizzes', quizzes);

app.use(function(err, req, res, next) {
    if(!err.status) {
        console.log(err);
        res.status(500).json({ success: false, message: 'Something went wrong' });
    } else {
        // console.log(err);
        res.status(err.status).json({ success: false, message: err.message });
    }
});

if(process.env.NODE_ENV === 'production') {
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'dist', 'index.html'));
    });
}

const server = require('http').createServer(app);
const io = require('socket.io')(server, {
    cors: {
      origin: 'http://localhost:4200',
      methods: ['GET', 'POST'],
      allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'Authorization'],
      credentials: true
    }
  });

registerSocketEvents(io);
require('./sockets/gameRoomLoader').loadGames();

server.listen(port, () => {
    console.log(`Server started at ${port}`);
});
