const {decode} = require('html-entities');

module.exports.decodeQuestion = question => {
    switch (question.question_model){
        case 'YesNoQuestion':
            return {
                ...question,
                question_data: {
                    ...question.question_data,
                    text: decode(question.question_data.text),
                }
            }
        case 'MultipleChoiceQuestion':
            return {
                ...question,
                question_data: {
                    ...question.question_data,
                    text: decode(question.question_data.text),
                    multiple_correct: question.question_data.multiple_correct,
                    options: question.question_data.options.map(qdb => {
                        return {option_text: decode(qdb.option_text), correct: qdb.correct};
                    })
                }
            }
        case 'DragDropQuestion':
            return {
                ...question,
                question_data: {
                    ...question.question_data,
                    text: decode(question.question_data.text),
                    items: question.question_data.items.map(item => {
                        return decode(item);
                    })
                }
            }
        default:
            console.log('Unknown question type!');
            return null;
    }
}