const User = require('../models/user');
const config = require('../config/auth.config');

const CATEGORY_MIXED = 0;

const categories = new Map();
categories.set(CATEGORY_MIXED, "Mixed");
categories.set(9 , "General Knowledge");
categories.set(10, "Books");
categories.set(11, "Film");
categories.set(12, "Music");
categories.set(13, "Musicals & Theatres");
categories.set(14, "Television");
categories.set(15, "Video Games");
categories.set(16, "Board Games");
categories.set(17, "Science & Nature");
categories.set(18, "Computers");
categories.set(19, "Mathematics");
categories.set(20, "Mythology");
categories.set(21, "Sports");
categories.set(22, "Geography");
categories.set(23, "History");
categories.set(24, "Politics");
categories.set(25, "Art");
categories.set(26, "Celebrities");
categories.set(27, "Animals");
categories.set(28, "Vehicles");
categories.set(29, "Comics");
categories.set(30, "Gadgets");
categories.set(31, "Japanese Anime & Manga");
categories.set(32, "Cartoon & Animations");

module.exports.timeLimit = 30;

module.exports.getUrl = (category, amount) => {
    let link = 'https://opentdb.com/api.php?';
    
    if(category != undefined && category != CATEGORY_MIXED){
        link += 'category=' + category + "&";
    }

    if(amount != undefined && amount >= 1 && amount <= 50){
        link += 'amount=' + amount;
    }else{
        link += 'amount=10';
    }

    return link;
}

module.exports.getCategories = () => {
    return Array.from(categories.entries());
}

module.exports.getCategoryName = categoryId => {
    return categories.get(Number(categoryId));
}

let author = null;

module.exports.getOpenTriviaAuthor = async () => {
    try{
        if (author == null){
            let user = await User.findOne({username: 'OpenTrivia'});
            if (user){
                author = user._id;
            }else{
                let newUser = new User({username: 'OpenTrivia', password: config.opent_password});
                let userDb = await newUser.save();
                author = userDb._id;
            }
        }
        return author;
    }catch(err){
        console.log(`Admin creation failed!`)
        console.log(err);
        return null;
    }
    
}