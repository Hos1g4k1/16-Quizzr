import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { AuthenticationService } from '../../services/authentication.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss', '../../form.scss']
})
export class RegisterComponent implements OnInit {

  signupForm: FormGroup;
  errorMessage: string;
  signupFail: boolean;

  constructor(
    private authService: AuthenticationService, 
    private router: Router,
    private formBuilder: FormBuilder
  ) { 
    this.signupForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {
  }

  register(): void {
    const username: string = this.signupForm.get('username').value;
    const password: string = this.signupForm.get('password').value;
    
    this.authService.register({ username, password } as User).subscribe(
      res => {
        this.authService.setUserData(res.token, res.user);
        this.router.navigate(['/home']);
        this.signupFail = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.signupFail = true;
      }
    )
  }

}
