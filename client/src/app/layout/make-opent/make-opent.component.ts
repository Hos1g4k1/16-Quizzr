import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { QuizService } from 'src/app/services/quiz.service';

@Component({
  selector: 'app-quiz-start',
  templateUrl: './make-opent.component.html',
  styleUrls: ['./make-opent.component.scss', '../../form.scss', '../../layout.scss', '../../quiz.scss']
})
export class MakeOpenTComponent implements OnInit {

  amount: number = 10;
  category: string = this.route.snapshot.queryParamMap.get('category');
  categoryName: string;

  errorMessage: string;

  dialogActive: boolean = false;

  dialogButtons: string[];
  dialogText: string;

  constructor(
    private authService: AuthenticationService,
    private quizService: QuizService,
    private router: Router,
    private route : ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.quizService.getCategoryName(this.category).subscribe(
      res => {
        this.categoryName = res.categoryName;
      },
      err => {
        this.router.navigate(['/home']);
      }
    );
  }

  openDialog(): void {
    this.dialogButtons = ['Yes', 'No'];
    this.dialogText = 'Would you like to lock this room?';
    this.dialogActive = true;
  }

  getChoice(grLocked: string) {
    this.startMultiplayer(grLocked === 'Yes');
  }

  startSingleplayer(): void {
    this.quizService.createOpenTrivia(Number(this.category), Number(this.amount)).subscribe((res: any) => {
      this.router.navigate([`/quiz/${res.quiz._id}`]);
    });
  }

  startMultiplayer(isLocked: boolean): void {
    if(isNaN(this.amount)) {
      this.errorMessage = 'Invalid number of questions';
    } else if(this.amount < 1 || this.amount > 50) {
      this.errorMessage = 'Number of questions must be between 1 and 50';
    } else {
      this.quizService.createOpenTrivia(Number(this.category), Number(this.amount)).subscribe((res: any) => {
        this.quizService.createGameroom(res.quiz._id, `${this.authService.user.username}'s room`, true, isLocked).subscribe(res => {
          this.router.navigate([`/gameroom/${res.gameroom_id}`]);
        });
      });
    }
  }

}
