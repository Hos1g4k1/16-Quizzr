import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['../../form.scss', '../../layout.scss','./dialog-box.component.scss']
})
export class DialogBoxComponent implements OnInit {

  @ViewChild('gamecode') gamecode: ElementRef;
  @ViewChild('pin') pin: ElementRef;

  @Input() text: string[] = [];
  @Input('buttons') buttonsText: string[] = [];
  @Input() hasGameCodeField: boolean = false;
  @Input() hasPinField: boolean = false;
  @Input() message: string = "";

  @Output() onBtnClick: EventEmitter<string> = new EventEmitter<string>();
  @Output() onDialogClose: EventEmitter<void> = new EventEmitter<void>();
  @Output() inputText: EventEmitter<string[]> = new EventEmitter<string[]>();

  constructor() { }

  ngOnInit(): void {
  }

  closeDialog($event: Event): void {
    if ($event.target['classList'][0] === 'background-cover' || $event.target['classList'][0] === 'dialog-close') {
      this.onDialogClose.emit();
    }
  }

  getBtnText($event: Event): void {
    this.onBtnClick.emit($event.target['innerText']);

    if (this.hasGameCodeField && this.gamecode.nativeElement.value !== '') {
      this.inputText.emit([this.gamecode.nativeElement.value, this.pin?.nativeElement.value || ""]);
      this.gamecode.nativeElement.value = '';
      this.pin.nativeElement.value = '';
    }
  }

}
