import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss', '../../form.scss']
})
export class SearchBoxComponent implements OnInit {

  @Output('search') onSearch = new EventEmitter<string>();
  searchForm: FormGroup;

  constructor(private formBuilder: FormBuilder) { 
    this.searchForm = this.formBuilder.group({
      query: ['']
    });
  }

  ngOnInit(): void {
  }

  search(): void {
    this.onSearch.emit(this.searchForm.get('query').value);
  }

}
