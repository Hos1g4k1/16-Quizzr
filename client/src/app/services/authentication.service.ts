import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

import { User } from '../models/user';
import { apiUrl } from '../environment';

interface AuthMessage {
  success: boolean,
  message: string,
  token?: string,
  user?: User
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public user: User = {username: 'user'};

  private usersUrl = `${apiUrl}/users`;
  private httpOptions = 
  {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient, private router: Router) { 
    this.getUser();
  }

  private setToken(token: string): void {
    localStorage.setItem('token', token);
  }

  public getToken(): string {
    return localStorage.getItem('token');
  }

  public createHeaders() :HttpHeaders {
    let token = this.getToken();
    if(token) {
      return new HttpHeaders({'Content-Type': 'application/json', 'Authorization': token});
    }
  }

  public login(user: User): Observable<AuthMessage> {
    return this.http.post<AuthMessage>(`${this.usersUrl}/login`, user, this.httpOptions);
  }

  public logout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

  public isLoggedIn(): boolean {
    let token = localStorage.getItem('token');
    return token !== null;
  }

  public register(user: User): Observable<AuthMessage> {
    return this.http.post<AuthMessage>(`${this.usersUrl}/register`, user, this.httpOptions);
  }

  public setUserData(token: string, user: User): void {
    this.setToken(token);
    this.user = user;
  }

  public getUser(): void {
    let token = localStorage.getItem('token');
    if(token !== null) {
      this.http.get<AuthMessage>(`${this.usersUrl}`, {headers: this.createHeaders()}).subscribe(
        res => {
          this.user = res.user;
          console.log(res);
        },
        err => {
          console.log(err);
        }
      )
    }
  }

  public uploadPhoto(photo: File): Observable<AuthMessage> {
    const formData = new FormData();
    formData.append('avatar', photo, photo.name);
    let headers = new HttpHeaders({'Authorization': this.getToken()});
    return this.http.post<AuthMessage>(`${this.usersUrl}/uploadPhoto`, formData, {headers});
  }

  public changePassword(old_password: string, new_password: string): Observable<AuthMessage> {
    const data = {
        old_password,
        new_password
    };
    return this.http.post<AuthMessage>(`${this.usersUrl}/changePassword`, data, {headers: this.createHeaders()});
  }
}
