import { Injectable } from '@angular/core';

import { serverUrl } from '../environment';

@Injectable({
  providedIn: 'root'
})
export class UiService {

  constructor() { }

  getPhoto(photoUrl) {
    return photoUrl ? `${serverUrl}/${photoUrl}` : 'assets/default.jpg';
  }

}
