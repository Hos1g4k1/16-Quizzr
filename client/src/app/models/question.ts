export interface Question {
    sequence_number: number;
    question_data: YesNoQuestion | MultipleChoiceQuestion;
}

export interface YesNoQuestion {
    text: string;
}

export interface MultipleChoiceQuestion {
    text: string;
    options: Array<string>;
    // multiple_correct: boolean;
}