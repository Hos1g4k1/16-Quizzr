import { Question } from "./question";
import { User } from "./user";

export interface Quiz {
    author: User;
    name: string;
    description: string;
    questions?: Array<Question>;
}