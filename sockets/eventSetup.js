const validateToken = require('../middleware/validateToken');
const registerSinglePlayerEvents = require('./singleplayerEvents');
const {registerGameRoomEvents, onUserLeft} = require('./gameRoomEvents');

const GameRoom = require('../models/gameroom');
const events = require('./eventNames');

// Rejoins a socket into any active room
// This is primarily needed for testing because the server gets restarted all the time
// But it might also be useful for production (in case server ever goes down for a short time)
const rejoinCurrentGame = async (socket) => {
    const userId = socket.user.id;
    const gamerooms = await GameRoom.find({});
    for (const gameroomDoc of gamerooms){
        if (gameroomDoc.participants.map(x => {return x.toString()}).includes(userId)){
            const gameroomId = gameroomDoc._id.toString();
            socket.join(gameroomId);
            socket.to(gameroomId).emit(events.GAMEROOM_QUIZ_CHAT_MSG_META_RECV,     {gameroom_id: gameroomId,
                                                                                    user: {username: socket.user.username, photo: socket.user.photo},
                                                                                    chat_message: `User ${socket.user.username} has just joined!`});
            socket.current_game_id = gameroomId;
            socket.emit(events.GAMEROOM_QUIZ_JOIN, {success: true, message: `You've been automatically rejoined to gameroom ${gameroomDoc.gameroom_name}`});
            return;
        }
   } 
}

module.exports = io => {

    io.use(async (socket, next) => {
        if (socket.handshake.query && socket.handshake.query.token){
            const token = socket.handshake.query.token.split(" ")[1];
            const validation = await validateToken(token);
            if (!validation.success){
                return next(new Error('Unauthorized.'));    
            }
            socket.user = validation.user;
            next();
        }
        else {
            return next(new Error('Unauthorized.'));
        }    
    });



    io.on('connection', socket => {
        console.log(`Client connected: ${socket.user.username}`);
        socket.inGame = false

        rejoinCurrentGame(socket);

        // TODO: Socket io most likely removes socket along with all its
        // data on disconnect, so this is probably unnecessary
        socket.on('disconnect', async () => {
            console.log('Client disconnected!');
            if (socket.hasOwnProperty('current_game_id')){
                console.log(`\t -- Emitting on user left`)
                await onUserLeft(socket, 2);
            }
        });
        registerSinglePlayerEvents(socket);
        registerGameRoomEvents(socket, io);

    });    


    
}