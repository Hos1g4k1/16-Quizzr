const GameRoom = require('../models/gameroom');
const eventNames = require('./eventNames');

let gamerooms = new Map();

module.exports.GAMEROOM_STATE = {
    IDLE                    :'idle',          
    AWAITING_ANSWERS        :'awaiting_answers',               
    COOLDOWN                :'cooldown',    
    DONE                    :'done'
};



module.exports.insertGameRoom = (gameroom) => {
    let gameroom_id = gameroom._id.toString();
    if (gamerooms.has(gameroom_id)){
        return {success: false, message: `This gameroom already exists!`};
    }

    gameRoomObject = {
        gameroom_id: gameroom_id,                
        gameroom_name: gameroom.gameroom_name,
        author: gameroom.author,
        participants: gameroom.participants,
        quiz: gameroom.quiz,
        started: false,
        curr_question_idx: -1,
        user_answers: null,         
        num_users: 0,
        state: this.GAMEROOM_STATE.IDLE
    }

    gamerooms.set(gameroom_id, gameRoomObject);
    return {success: true};
}

module.exports.getGameRoom = (gameroom_id) => {
    return gamerooms.get(gameroom_id);
}

module.exports.setGameRoom = (id, obj) => {
    gamerooms.set(id, obj);
}

module.exports.allGameRooms = async () => {
    return gamerooms;
}

module.exports.deleteGameRoom = async (gameroom_id) => {
    return gamerooms.delete(gameroom_id);
}



