const validateToken = require('./validateToken');

module.exports.required = async (req, res, next) => {
    // NOTE: Server used to crash if token not provided at all
    if(!req.headers.authorization || typeof req.headers.authorization === undefined){
        return res.status(401).json({success: false, message: "Unauthorized"});
    }
    const token = req.headers.authorization.split(" ")[1];
    const validation = await validateToken(token);
    if (!validation.success){
        res.status(401).json({ success: false, message: "Unauthorized" });
        return;  
    }
    req.user = validation.user;
    next();  
};