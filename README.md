# Project Quizzr

Quizzr is a web app for quiz competitions. Quizzes consist of different types of questions (yes/no, multiple choice, drag & drop, ...). Questions also have time limits - faster you answer higher the score. Players can train alone in singleplayer mode or compete with others in multiplayer mode using gamerooms. Gamerooms can be public, in which case anyone can join, or PIN-protected. Custom quiz creating is supported, as well as generating random quizzes using OpenTriviaDB.

# Database
MongoDB running at `localhost:27017`. 

If you have Mongo installed locally you can use `npm run mongo` on Linux or just start `mongod.exe` on Windows.

Or using Docker:
```bash
npm run dbmake
npm run dbstart
```

# Build for production
Install required dependencies in the server and client folders using:
```
npm install
```
You can build the client from the root folder using:
```
npm run build --prefix client
```
Afterwards use: 
```
npm run start
```

# Build for development
Install required dependencies in the server and client folders using:
```
npm install
```
Start the server:
```
npm run dev
```
Start the client:
```
npm run start --prefix client
```

# Database schemas
**User:**
| Field      | Type | Description |
| ----------- | ----------- |----------- |
| username      | String   |        |
| password   | String        |    Password hash        |
| photo   | String        |    Profile photo url        |

**Question:**
| Field      | Type | Description |
| ----------- | ----------- |----------- |
| quiz      | Id   | The quiz that the question belongs to        |
| sequence_number   | Number        |    Index of the question within its quiz |
| question_data   | Id        |  Specific data about the question  |
| question_model   | String        |  Model name for the specific question type  |
| time_limit   | Number        |  Time limit of the question (in seconds)  |

**YesNoQuestion:**
| Field      | Type | Description |
| ----------- | ----------- |----------- |
| text      | String   | The question text        |
| answer   | Boolean        |    The correct answer        |

**MultipleChoiceQuestion:**
| Field      | Type | Description |
| ----------- | ----------- |----------- |
| text      | String   | The question text        |
| multiple_correct   | Boolean        |    Flag indicating whether more than one answer is correct    |
| options   | Array[(String, Boolean)]        |    Choices and correct indicators    |

**DragDropQuestion:**
| Field      | Type | Description |
| ----------- | ----------- |----------- |
| text      | String   | The question text        |
| items   | Array[String]        |    Choices in correct order    |

**Quiz:**
| Field      | Type | Description |
| ----------- | ----------- |----------- |
| author      | Id   | The user who created the quiz        |
| name   | String        |        |
| description   | String        |        |
| photo   | String        |     Quiz photo url    |
| questions   | Array[Id]        |     Questions within this quiz    |


**GameRoom:**
| Field      | Type | Description |
| ----------- | ----------- |----------- |
| gameroom_name      | String   |        |
| author   | Id        | The user who created the gameroom        |
| quiz   | Id        | The quiz that's being played        |
| participants   | Array[Id]        | Users in the gameroom        |
| multiplayer   | Boolean        |     Whether it's a multiplayer game    |
| locked   | Boolean        |     Whether the room is PIN-protected    |
| game_code   | Number        |     Unique 6-digit code identifying the room |
| pin   | Number        |      |

# Video
A video showing the main features can be found [here](https://www.youtube.com/watch?v=oA0_DkH6GRc).


## Developers

- [Aleksa Kojadinović, 130/2017](https://gitlab.com/aleksakojadinovic)
- [Tatjana Kunić, 139/2017](https://gitlab.com/kunict11)
- [Vladimir Vuksanović, 145/2017](https://gitlab.com/VladimirV99)
- [Lazar Čeliković, 259/2017](https://gitlab.com/Hos1g4k1)
