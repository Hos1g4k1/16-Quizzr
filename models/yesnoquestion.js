const mongoose = require('mongoose');
mongoose.Promise = global.Promise;


const YesNoQuestionSchema = mongoose.Schema({
    text: {
        type: String,
        required: true
    },
    answer: {
        type: Boolean,
        required: true
    }
});

const YesNoQuestion = module.exports = mongoose.model('YesNoQuestion', YesNoQuestionSchema);

module.exports.create = (question) => {
    return new Promise((resolve, reject) => {
        let newQuestion = new YesNoQuestion(question);
        newQuestion
            .save()
            .then(question => {
                resolve({success: true, status: 201, message: 'Yes/No Question created successfully.', question});
            })
            .catch(err => {
                reject({success: false, status: 500, message: 'Something went wrong.'});
            })
    });
};

module.exports.edit = (question) => {
    return new Promise((resolve, reject) => {

        let obj = {"text": question.text, "answer": question.answer};

        YesNoQuestion.findByIdAndUpdate(question._id, obj, {new: true})
            .then(question => {
                resolve({success: true, status: 200, message: 'Yes/No Question updated successfully.', question});
            })
            .catch(err => {
                reject({success: false, status: 500, message: 'Something went wrong.'});
            })
    });
};